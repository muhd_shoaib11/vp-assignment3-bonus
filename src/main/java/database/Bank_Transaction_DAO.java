package database;

import java.util.List;

public interface Bank_Transaction_DAO {
	public List<Bank_Transaction> getAllBankTransactions();
	public Bank_Transaction getBankTransactionByID(Integer trans_id);
	public Bank_Transaction createBankTransaction();
	public boolean updateBankTransaction(Integer trans_id, Bank_Transaction transaction);
	public boolean removeBankTransactionByID(Integer trans_id);
	public boolean removeAllBankTransactions();
}
