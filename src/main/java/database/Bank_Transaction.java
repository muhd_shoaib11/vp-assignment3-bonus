package database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Bank_Transaction {
	@Id
	private Integer trans_id;
	@Column(name = "account_id")
	private Integer account_id; 
	@Column(name = "type")
	private String type;  
	@Column(name = "operation")
	private String operation;  
	@Column(name = "amount")
	private Float amount;  
	@Column(name = "balance")
	private Float balance;  
	@Column(name = "k_symbol")
	private String k_symbol;  
	@Column(name = "bank")
	private String bank;  
	@Column(name = "account")
	private Integer account;  
	
	/**
	 * @return the trans_id
	 */
	Integer getTrans_id() {
		return trans_id;
	}

	/**
	 * @param trans_id the trans_id to set
	 */
	void setTrans_id(Integer trans_id) {
		this.trans_id = trans_id;
	}

	/**
	 * @return the account_id
	 */
	Integer getAccount_id() {
		return account_id;
	}

	/**
	 * @param account_id the account_id to set
	 */
	void setAccount_id(Integer account_id) {
		this.account_id = account_id;
	}

	/**
	 * @return the account
	 */
	Integer getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	void setAccount(Integer account) {
		this.account = account;
	}

	/**
	 * @return the type
	 */
	String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the operation
	 */
	String getOperation() {
		return operation;
	}

	/**
	 * @param operation the operation to set
	 */
	void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * @return the amount
	 */
	Float getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	void setAmount(Float amount) {
		this.amount = amount;
	}

	/**
	 * @return the balance
	 */
	Float getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	void setBalance(Float balance) {
		this.balance = balance;
	}

	/**
	 * @return the k_symbol
	 */
	String getK_symbol() {
		return k_symbol;
	}

	/**
	 * @param k_symbol the k_symbol to set
	 */
	void setK_symbol(String k_symbol) {
		this.k_symbol = k_symbol;
	}

	/**
	 * @return the bank
	 */
	String getBank() {
		return bank;
	}

	/**
	 * @param bank the bank to set
	 */
	void setBank(String bank) {
		this.bank = bank;
	}

}