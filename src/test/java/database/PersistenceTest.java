package database;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import junit.framework.TestCase;

/**
 * Unit tests for Persistence.
 */

@RunWith(JUnit4.class)
public class PersistenceTest extends TestCase {
	
	static final String serverName = "WINDOWS-FUMCC90";
	static final String instanceName = "SQLEXPRESS";
	static Session session;
	
    @Before
    public void setUpDatabase() throws Exception {
    	String hibernateConnectionUrl = "jdbc:sqlserver://"+serverName+"\\"+instanceName+";databaseName=BANK_TRANSACTIONS;integratedSecurity=true;";
		session = new Configuration().configure("hibernate.cfg.xml").setProperty("hibernate.connection.url", hibernateConnectionUrl).buildSessionFactory().openSession();
        session.beginTransaction();
		List<String> transactions = Files.readAllLines(Paths.get("Bank_Transactions.csv"));
		int count=0;
		for(String transaction : transactions) {
			try {
				String[] t = transaction.split(",");
				Bank_Transaction bt = new Bank_Transaction();
				bt.setTrans_id(Integer.valueOf(t[0]));
				bt.setAccount_id(Integer.valueOf(t[1]));
				bt.setType(String.valueOf(t[3]));
				bt.setOperation(String.valueOf(t[4]));
				bt.setAmount(Float.valueOf(t[5]));
				bt.setBalance(Float.valueOf(t[6]));
				bt.setK_symbol(String.valueOf(t[7]));
				bt.setBank(String.valueOf(t[8]));
				bt.setAccount(Integer.valueOf(t[9]));
			    session.persist(bt);
			}
			catch (Exception e) {
				
			}
		    if ( ++count % 50 == 0 ) {
		        session.flush();
		        session.clear();
		    }
			
		}
		session.getTransaction().commit();
    }

	@Test
    public void TestGetAllBankTransactions() throws Exception {
		int beforeResultSetSize = 158;
		List<Bank_Transaction> bankTransactions = null;
		try {
			session.beginTransaction();
			bankTransactions = new Bank_Transaction_DAO_Impl().getAllBankTransactions();
			session.persist(bankTransactions);
			session.getTransaction().commit();
			System.out.println("\n.......All Bank Transactions retrieved successfully from the database.......\n");
		} catch(Exception sqlException) {
			if(session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
		} finally {
			if(session != null) {
				session.close();
				assertEquals(bankTransactions.size(), beforeResultSetSize);
			}
		}
	}
	
	@Test
    public void TestGetBankTransactionByID() throws Exception {
		Bank_Transaction bankTransaction = null;
		try {
			session.beginTransaction();
			bankTransaction = new Bank_Transaction_DAO_Impl().getBankTransactionByID(279);
			session.persist(bankTransaction);
			session.getTransaction().commit();
			System.out.println("\n.......Bank Transaction retrieved successfully from the database.......\n");
		} catch(Exception sqlException) {
			if(session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
		} finally {
			if(session != null) {
				session.close();
				assertEquals((int)bankTransaction.getTrans_id(), 279);
			}
		}
	}
	
	@Test
    public void TestAddBankTransaction() throws Exception {
		int beforeResultSetSize = 158;
		List<Bank_Transaction> bankTransactions = null;
		try {
			session.beginTransaction();
			Bank_Transaction bankTransaction = new Bank_Transaction_DAO_Impl().createBankTransaction();
			session.persist(bankTransaction);
			session.getTransaction().commit();
			bankTransactions = new Bank_Transaction_DAO_Impl().getAllBankTransactions();
			System.out.println("\n.......Bank Transaction saved successfully in the database.......\n");
		} catch(Exception sqlException) {
			if(session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
		} finally {
			if(session != null) {
				session.close();
				assertEquals(bankTransactions.size(), beforeResultSetSize+1);
			}
		}
	}
	
	@Test
    public void TestUpdateTransactions() throws Exception {
		
	}

	@Test
	public void TestRemoveBankTransactionByID() {
		int beforeResultSetSize = 158;
		List<Bank_Transaction> bankTransactions = null;
		try {
			session.beginTransaction();
			boolean removed = new Bank_Transaction_DAO_Impl().removeBankTransactionByID(279);
			session.getTransaction().commit();
			bankTransactions = new Bank_Transaction_DAO_Impl().getAllBankTransactions();
			System.out.println("\n.......Bank Transaction deleted successfully from the database.......\n");
		} catch(Exception sqlException) {
			if(session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
		} finally {
			if(session != null) {
				session.close();
				assertEquals(bankTransactions.size(), beforeResultSetSize-1);
			}
		}
	}

	@Test
	public void TestRemoveAllBankTransactions() {
		List<Bank_Transaction> bankTransactions = null;
		try {
			session.beginTransaction();
			boolean removed = new Bank_Transaction_DAO_Impl().removeAllBankTransactions();
			session.getTransaction().commit();
			bankTransactions = new Bank_Transaction_DAO_Impl().getAllBankTransactions();
			System.out.println("\n.......All Bank Transaction deleted successfully from the database.......\n");
		} catch(Exception sqlException) {
			if(session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
		} finally {
			if(session != null) {
				session.close();
				assertEquals(bankTransactions.size(), 0);
			}
		}
	}
	
	@After
	public void cleanup() {
		session.close();
	}
	
}



